/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication()
	: mRotate(.13)
	, mMove(250)
	, mCamNode(0)
	, mDirection(Ogre::Vector3::ZERO)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication()
{
}
//---------------------------------------------------------------------------
void TutorialApplication::createScene()
{
	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(Ogre::ColourValue(.2, .2, .2));

	// Create an instance of a computer graphic model
	Ogre::Entity* tudorEntity = mSceneMgr->createEntity("tudorhouse.mesh");

	// Create an object in the scene
	Ogre::SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode("TudorNode");

	// Add the entity to the node to display the mesh in the scene
	node->attachObject(tudorEntity);

	// Create a directional light that gives shadows
	Ogre::Light* light = mSceneMgr->createLight("Light1");
	light->setType(Ogre::Light::LT_POINT);

	// Set the position of the directional light
	light->setPosition(Ogre::Vector3(300, 150, 0));

	// Color of light that reflects off scene models
	light->setDiffuseColour(Ogre::ColourValue::White);

	// Shininess of light that reflects off scene models
	light->setSpecularColour(Ogre::ColourValue::White);

	// Create the first child object in the scene which a camera will be attached
	node = mSceneMgr->getRootSceneNode()->createChildSceneNode("CamNode1", Ogre::Vector3(1200, -370, 0));

	// Rotate the node to the left by 90 degrees
	node->yaw(Ogre::Degree(90));
 
	// Assign camera object pointer to the first child object
	mCamNode = node;

	// Attach the camera to the first child object
	node->attachObject(mCamera);

	// Create the second child object in the scene which a camera will be attached
	node = mSceneMgr->getRootSceneNode()->createChildSceneNode("CamNode2", Ogre::Vector3(-500, -370, 1000));

	// Rotate the node to the right by 30 degrees
	node->yaw(Ogre::Degree(-30));
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
	// Call the BaseApplication virtual function because we still want its functionality
	bool ret = BaseApplication::frameRenderingQueued(fe);
 
	// Change the position of the camera object according to playerinput
	mCamNode->translate(mDirection * fe.timeSinceLastFrame, Ogre::Node::TS_LOCAL);
 
	return ret;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyPressed(const OIS::KeyEvent& ke)
{
	// Process key pressed input
	switch (ke.key)
	{
		// Process the escape key
		case OIS::KC_ESCAPE:

			// Set the program shutdown flag
			mShutDown = true;
			break;
 
		// Process the 1 key
		case OIS::KC_1:

			// Detach the camera from the current node to which it's attached
			mCamera->getParentSceneNode()->detachObject(mCamera);

			// Get the first child scene node
			mCamNode = mSceneMgr->getSceneNode("CamNode1");

			// Attach the camera to the first child scene node
			mCamNode->attachObject(mCamera);
			break;
 
		// Process the 2 key
		case OIS::KC_2:

			// Detach the camera from the current node to which it's attached
			mCamera->getParentSceneNode()->detachObject(mCamera);

			// Get the second child scene node
			mCamNode = mSceneMgr->getSceneNode("CamNode2");

			// Attach the camera to the second child scene node
			mCamNode->attachObject(mCamera);
			break;
 
		// Process the up and W key
		case OIS::KC_UP:
		case OIS::KC_W:

			// Move the camera forward
			mDirection.z = -mMove;
			break;
 
		// Process the down and S key
		case OIS::KC_DOWN:
		case OIS::KC_S:

			// Move the camera backward
			mDirection.z = mMove;
			break;
 
		// Process the left and A key
		case OIS::KC_LEFT:
		case OIS::KC_A:

			// Move the camera to the left
			mDirection.x = -mMove;
			break;
 
		// Process the right and D key
		case OIS::KC_RIGHT:
		case OIS::KC_D:

			// Move the camera to the right
			mDirection.x = mMove;
			break;
 
		// Process the page down and E key
		case OIS::KC_PGDOWN:
		case OIS::KC_E:

			// Move the camera down
			mDirection.y = -mMove;
			break;
 
		// Process the page up and Q key
		case OIS::KC_PGUP:
		case OIS::KC_Q:

			// Move the camera up
			mDirection.y = mMove;
			break;
 
		// Process all other keys
		default:

			// Do nothing
			break;
	}

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyReleased(const OIS::KeyEvent& ke)
{
	// Process key released input
	switch (ke.key)
	{
		// Process the up and W key
		case OIS::KC_UP:
		case OIS::KC_W:

			// Stop camera movement forward
			mDirection.z = 0;
			break;
 
		// Process the down and S key
		case OIS::KC_DOWN:
		case OIS::KC_S:

			// Stop camera movement backward
			mDirection.z = 0;
			break;

		// Process the left and A key
		case OIS::KC_LEFT:
		case OIS::KC_A:

			// Stop camera movement to the left
			mDirection.x = 0;
			break;

		// Process the right and D key
		case OIS::KC_RIGHT:
		case OIS::KC_D:

			// Stop camera movement to the right
			mDirection.x = 0;
			break;
 
		// Process the page down and E key
		case OIS::KC_PGDOWN:
		case OIS::KC_E:

			// Stop camera movement down
			mDirection.y = 0;
			break;
 
		// Process the page up and Q key
		case OIS::KC_PGUP:
		case OIS::KC_Q:

			// Stop camera movement up
			mDirection.y = 0;
			break;

		// Process all other keys
		default:

			// Do nothing
			break;
	}
 
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseMoved(const OIS::MouseEvent& me)
{
	// Process mouse movement if the right mouse button is pressed
	if (me.state.buttonDown(OIS::MB_Right))
	{
		// Rotate the camera right or left
		mCamNode->yaw(Ogre::Degree(-mRotate * me.state.X.rel), Ogre::Node::TS_WORLD);

		// Move the camera up or down
		mCamNode->pitch(Ogre::Degree(-mRotate * me.state.Y.rel), Ogre::Node::TS_LOCAL);
	}
 
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Get the directional light
	Ogre::Light* light = mSceneMgr->getLight("Light1");
 
	// Process the mouse button pressed input
	switch (id)
	{
		// The left mouse button
		case OIS::MB_Left:

			// Toggle light visibility
			light->setVisible(!light->isVisible());
			break;

		// Process all other mouse buttons (right and middle)
		default:

			// Do nothing
			break;
	}

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Process the mouse button released input

	// Do nothing

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		} catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
			e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
