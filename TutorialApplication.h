/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include "BaseApplication.h"

//---------------------------------------------------------------------------
class TutorialApplication : public BaseApplication
{
public:
	TutorialApplication();
	virtual ~TutorialApplication();
 
private:
	virtual void createScene();
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& fe);

	// Callback method for when a key is pressed
	virtual bool keyPressed(const OIS::KeyEvent& ke);
	
	// Callback method for when a key is released
	virtual bool keyReleased(const OIS::KeyEvent& ke);
 
	// Callback method for when the mouse is moved
	virtual bool mouseMoved(const OIS::MouseEvent& me);

	// Callback method for when mouse buttons are pressed
	virtual bool mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id);
	
	// Callback method for when mouse buttons are released
	virtual bool mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id);
 
	// Amount to rotate node objects in the scene
	Ogre::Real mRotate;

	// Distance to move node objects in the scene
	Ogre::Real mMove;

	// Node object to attach the camera
	Ogre::SceneNode* mCamNode;

	// Direction to move node objects in the scene
	Ogre::Vector3 mDirection; 
};
//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------
